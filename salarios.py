n_empleados = int(input("¿Cúantos empleados hay?:"))
nómina_total = 0
valor_total = 0

for n in range(0, n_empleados):
    nómina = int(input("¿Cúal es su nómina?:"))
    impuesto = int(input("¿Cúal es el porcentaje de su impuesto?:"))
    año = input("¿Tiene más de un año de antiguedad?: ")
    valor = nómina * (impuesto/100)
    nómina_total = nómina_total + nómina

    if (año == "si"):
        coste_empresa = valor - (0.1*impuesto)
        valor_total += coste_empresa
    else:
        valor_total += valor

print("Nómina total: ", nómina_total)
print("Coste total: ", valor_total)